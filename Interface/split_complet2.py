from datetime import datetime
import os
import export_complet2

startTime = datetime.now()
try:
    os.mkdir('data')
except :
    pass
filename = "data/export.csv"

def supp_fichier(fic):
    try:
        os.remove(fic)
    except OSError:
        pass

fichiers = ["data/author.csv", "data/auteur2.csv", "data/keyword.csv"
            , "data/keywords2.csv", filename, "data/export2.csv", "data/publication.csv"
            , "data/venue.csv", "data/publication_author.csv", "data/publication_keywords.csv"
            ,"data/publication_venue.csv", "data/publication_year.csv", "data/year.csv"]

fichiers_fin = ["data/auteur2.csv", "data/keywords2.csv"]

for i in fichiers :   
    supp_fichier(i)

    
def importer(nomfichier):
    import csv

    from lxml import etree as ET
    import nltk
    from nltk.corpus import stopwords
    from nltk.tokenize import RegexpTokenizer
    from collections import Counter

    #genere les stopwords (mots ne donnant pas d'informations du style les déterminants, les conjonctions)
    stop_words = stopwords.words('english')
    stop_words_fr = stopwords.words('french')
    nltk.download("stopwords")
    tokenizer = RegexpTokenizer(r'\w+')
    
    #initialisation du parser 
    tree = ET.iterparse(nomfichier, events = ('end',)
                        , tag = ('article', 'book', 'incollection', 'inproceedings'
                                 , 'proceedings', 'phdthesis', 'masterthesis', 'www'
                                 ,'person', 'data')
                        , dtd_validation = True, recover = True)
    
    
    #creation des header des differents fichiers csv 
    fieldnames_aut = ["author"]
    fieldnames_key = ["keywords"]
    fieldnames_publication = ["id_publication", "date_pub", "article_title", "categorie", "nbr_authors"]
    fieldnames_year = ["year", "id_year"]
    fieldnames_publi_author = ["author", "id_publication"]
    fieldnames_publi_keywords = ["id_publication", "keyword", "nbr_use_keyword"]
    fieldnames_publi_venue = ["id_publication", "id_venue"]
    fieldnames_publi_year = ["id_publication", "id_year"]

    auth,keyw,publi,venue,year = ({} for i in range(5))

    publi_author,publi_keywords,publi_venue,publi_year =  ({} for i in range(4))

    nb_auteur = 0
      
    ls, ls_aut, ls_keywords, ls_publication, ls_venue, ls_year, ls_venue_id = ([]for i in range(7))

    ls_publi_author, ls_publi_keywords, ls_publi_venue, ls_publi_year = ([]for i in range(4))

    tmp_keywords = []
    tmp_publi_keywords = []
    tmp_publi_author = []
    tmp_dict_keywords = {}
    
    #boucle qui va parcourir chaque balise du fichier xml
    for event, elem in tree:
        ls.append(["type",[elem.tag]])
        
        #La fonction items permet de récupérer les informations écrites à l'intérieur des balises
        #Exemple: <article key ="bla"> elle récupère key : bla dans un dictionnaire
        for i in range (0,len(elem.items())):
            #On stocke cette information dans une liste
            ls.append([elem.items()[i][0], elem.items()[i][1]])
        
        #On stocke le nom de la balise et ce qu'elle contient sous forme de dictionnaire
        for i in elem :
            ls.append([i.tag,i.text])

        #Une fois tout l'article enregistre dans ls on va creer les differents dictionnaires
            #que l'on ecrira dans csv
        for i in ls:
            
            # si l'element de ls est un auteur on rajoute le nom de l'auteur dans un dictionnaire
            # qui sera une ligne du fichier auteur csv. On incremente le nombre d'auteur que l'on ecrira aussi
            # dans ce fichier csv. On sauvegarde toutes ces informations dans une liste ce qui nous permet
            # de reduire grandement le temps de calcul
            if i[0] == "author":
                auth[i[0]] = [i[1]]            
                nb_auteur += 1
                tmp_publi_author.append(i[1])
                                
            if i[0] == "key":
                publi["id_publication"] = [i[1]]
                publi_keywords["id_publication"] = [i[1]]
                tmp_key = i[1]
                publi_venue["id_publication"] = [i[1]]
                publi_year["id_publication"] = [i[1]]
                
            if i[0] == "title" :
                if i[1] is not None :
                    #on transforme la phrase title en chaine de mots
                    tmp =  tokenizer.tokenize(i[1])
                    for j in tmp :
                        #Pour chaque mot on increment a une liste s'il n'est pas un stopword
                        if j.lower() not in stop_words_fr and j.lower() not in stop_words :
                            tmp_keywords.append(j.upper())
                            tmp_publi_keywords.append(j.upper())
                                                        
                publi["article_title"] = [i[1]]
            
            if i[0] == "mdate":
                publi["date_pub"] = [i[1]]
            
            if i[0]  == "type":
                publi["categorie"] = i[1]
            
            if i[0] == "journal":
                #on genere une liste qui contient tous les lieux de publication unique
                if i[1] not in ls_venue :
                    ls_venue.append(i[1])
                publi_venue["id_venue"] = ["id_" + i[1]]
                
            if i[0] == "year":
                publi_year["id_"+i[0]] = ["id_" + i[1]]   
                
                #creation de l'id year 
                if len(ls_year) == 0  :
                    
                    year[i[0]] = i[1]
                    year["id_" + i[0]] = "id_" + i[1]
                    tmp_year = [i[1]]
                    ls_year.append(year)
                    
                if i[1] not in tmp_year :
                    year[i[0]] = i[1]
                    year["id_" + i[0]] = "id_" + i[1]
                    tmp_year.append(i[1])
                    ls_year.append(year)

            year = {}
       
        publi["nbr_authors"] = str(nb_auteur)         
        
        # on cree le dictionnaire qui contiendra en clef un keyword et en value son nombre d'utilisation
        for i in tmp_publi_keywords :
            tmp_dict_keywords[i] = tmp_publi_keywords.count(i)

        for i in tmp_keywords :
            keyw["keywords"] = [i]
            ls_keywords.append([keyw])
            keyw = {}


        for i in tmp_publi_author :
            publi_author["id_publication"] = [tmp_key]
            publi_author["author"] = [i]
            ls_publi_author.append([publi_author])
            publi_author = {}
        tmp_publi_author = []


        for i in tmp_dict_keywords.keys():
            
            publi_keywords["id_publication"] = [tmp_key]
            publi_keywords["keyword"] = [i]
            publi_keywords["nbr_use_keyword"] = [str(tmp_dict_keywords[i])]

            ls_publi_keywords.append([publi_keywords])
            publi_keywords = {}
            
                
        tmp_dict_keywords = {}

        tmp_keywords = []
        tmp_publi_keywords = []
        
            
        
        ls = []
        ls_aut.append([auth])
        ls_publication.append([publi])
        ls_publi_venue.append([publi_venue])
        ls_publi_year.append([publi_year])

        auth = {}
        keyw = {}
        publi = {}
        publi_venue = {}
        publi_year = {}
        nb_auteur = 0
        elem.clear()
        
        #Une fois que la liste est supérieur au nb indiqué on écrit dans le csv et on vide la liste
        #Cette manip permet de diviser le temps de calcul par environ 7
        if len(ls_aut) > 10000 :
            exporter(ls_aut, fieldnames_aut, "data/auteur2.csv")
            ls_aut = []

        if len(ls_keywords) > 50000 :
            exporter(ls_keywords, fieldnames_key, "data/keywords2.csv")
            ls_keywords = []

        if len(ls_publication) > 5000 :
            exporter(ls_publication, fieldnames_publication, "data/publication.csv")
            ls_publication = []
            
        if len(ls_publi_author) > 10000 :
            exporter(ls_publi_author, fieldnames_publi_author, "data/publication_author.csv")
            ls_publi_author = []

        if len(ls_publi_keywords) > 100000 :
            exporter(ls_publi_keywords, fieldnames_publi_keywords , "data/publication_keywords.csv")
            ls_publi_keywords = []

        if len(ls_publi_venue) > 10000 :
            exporter(ls_publi_venue, fieldnames_publi_venue , "data/publication_venue.csv")
            ls_publi_venue = []
        
        if len(ls_publi_year) > 10000 :
            exporter(ls_publi_year, fieldnames_publi_year , "data/publication_year.csv")
            ls_publi_year = []
            
    if len(ls_keywords) > 0 :
        exporter(ls_keywords, fieldnames_key, "data/keywords2.csv")
        ls_keywords = []

    if len(ls_publication) > 0 :
        exporter(ls_publication, fieldnames_publication, "data/publication.csv")
        ls_publication = []
        
    if len(ls_publi_author) > 0 :
        exporter(ls_publi_author, fieldnames_publi_author, "data/publication_author.csv")
        ls_publi_author = []

    if len(ls_publi_keywords) > 0 :
        exporter(ls_publi_keywords, fieldnames_publi_keywords , "data/publication_keywords.csv")
        ls_publi_keywords = []

    if len(ls_publi_venue) > 0 :
        exporter(ls_publi_venue, fieldnames_publi_venue , "data/publication_venue.csv")
        ls_publi_venue = []
    
    if len(ls_publi_year) > 0 :
        exporter(ls_publi_year, fieldnames_publi_year , "data/publication_year.csv")
        ls_publi_year = []


    for i in ls_venue :
        ls_venue_id.append("id_"+ i)
    
    venue["name_venue"] = ls_venue
    venue["id_venue"] = ls_venue_id
    
    with open("data/venue.csv", "w", newline = "", encoding="utf-16") as csvfile:
        writer = csv.writer(csvfile, delimiter = ";")
        writer.writerow(["id_venue", "name_venue"])
        writer.writerows(zip(*venue.values()))
        
    with open('data/year.csv', 'w', newline='', encoding="utf-16")  as output_file:
        dict_writer = csv.DictWriter(output_file, fieldnames_year)
        dict_writer.writerows(ls_year) 
        
    with open('data/auteur2.csv', 'r', encoding="utf-16") as read_obj, \
            open('data/author.csv', 'a', newline='', encoding="utf-16") as write_obj:
        csv_reader = csv.reader(read_obj)
        csv_writer = csv.DictWriter(write_obj, ["author", "nbr_publication"], delimiter = ";")
        csv_writer.writeheader()

        x = Counter()

        for row in csv_reader:
            if row[0] != "" :
                x[row[0]] += 1
        
        for key in x:
            csv_writer.writerow({'author': key, 'nbr_publication': x[key]})
            
    with open('data/keywords2.csv', 'r', encoding="utf-16") as read_obj, \
            open('data/keyword.csv', 'a', newline='', encoding="utf-16") as write_obj:
        csv_reader = csv.reader(read_obj)
        csv_writer = csv.DictWriter(write_obj, ["keyword", "nbr_utilisation"], delimiter = ";")
        csv_writer.writeheader()
        x = Counter()

        for row in csv_reader:
            if row[0] != "" :
                x[row[0]] += 1
        
        for key in x:
            csv_writer.writerow({'keyword': key, 'nbr_utilisation': x[key]})

        
    for i in fichiers_fin :   
        supp_fichier(i)
    return (ls)

def exporter(doc, tags, filename):
    import csv

    with open(filename, 'a', newline ='', encoding="utf-16") as csvfile:
        writer = csv.DictWriter(csvfile, tags, delimiter = ";")
        if os.stat(filename).st_size == 0 :
           writer.writeheader()
        for i in doc:
            for row in i :
                val = dict((k, ",".join(map(str,v))) for k, v in row.items())
                writer.writerow(val)


#doc = importer("dblp_100mo.xml")
#export_complet2.importer("dblp_100mo.xml")
print("Temps d'éxecution : ", datetime.now() - startTime)   


            
