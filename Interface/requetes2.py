# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 22:03:33 2020

@author: Bassat
"""
import os

def supp_fichier(fic):
    try:
        os.remove(fic)
    except OSError:
        pass

fichiers = ["author.csv", "auteur2.csv", "keyword.csv"
            , "keywords2.csv", "export2.csv", "publication.csv"
            , "venue.csv", "publication_author.csv", "publication_keywords.csv"
            ,"publication_venue.csv", "publication_year.csv", "year.csv"]

fichiers_fin = ["auteur2.csv", "keywords2.csv"]

for i in fichiers :   
    supp_fichier(i)


def test(critere1, critere2):
    
    mdict = {}
    
    fieldnames = ["author","title","venue", "id_publication", "mdate", "type",
                  "keywords", "id_venue", "id_author", "year", "id_year"]

    fieldnames_aut = ["author"]
    fieldnames_key = ["keywords"]
    fieldnames_publication = ["id_publication", "mdate", "title", "type"]
    fieldnames_year = ["year", "id_year"]
    fieldnames_venue = ["venue", "id_venue"]
    fieldnames_publi_author = ["author", "id_publication"]
    fieldnames_publi_keywords = ["id_publication", "keywords","nbr_use_keyword"]
    fieldnames_publi_venue = ["id_publication", "id_venue"]
    fieldnames_publi_year = ["id_publication", "id_year"]
    
    auth,keyw,publi,venue,year = ({} for i in range(5))

    publi_author,publi_keywords,publi_venue,publi_year =  ({} for i in range(4))

    ls, ls_aut, ls_keywords, ls_publication, ls_venue, ls_year, ls_venue_id = ([]for i in range(7))
    
    ls_publi_author, ls_publi_keywords, ls_publi_venue, ls_publi_year = ([]for i in range(4))

    tmp_nb_key = {}
    tmp_year = []
    tmp_venue = []
    tmp_key = []
    tmp_aut = []
    tmp_dict_keywords = {}
    tmp_dict_aut = {}
    col_imp = ["author","title","mdate","type","venue","id_publication", "year"]
    nom_col = ["auteur", "titre", "date de publication", "type","lieu de publication", "identifiant_publication", "année"]
    
    import os
    
    filename = "export.csv"
    filename2 = "export2.csv"
    
    import csv
    
    try:
        os.remove(filename2)
    except OSError:
        pass

    with open('export2.csv', 'a', newline ='', encoding="utf-16") as csvfile:
        writer = csv.writer(csvfile, delimiter = ";")
        writer.writerow(nom_col)
    
    with open(filename, 'r', encoding = "utf-16") as f:
    
        reader = csv.DictReader(f, fieldnames = fieldnames, delimiter = ";")
        
        if critere1 == "author":
            for row in reader:
                if critere2 in row["author"]:
                    for i in col_imp:
                        mdict[i] = row[i]
                    
                    with open('export2.csv', 'a', newline ='', encoding="utf-16") as csvfile:
                         writer = csv.DictWriter(csvfile, ["author","title","mdate","type","venue","id_publication", "year"], delimiter = ";")
                         writer.writerow(mdict)
  
                    for i in fieldnames_aut:   
                        nb_aut = len(row[i].split(","))
                        
                    for j in row[i].split(","):
                        tmp_aut.append(j)

                    for x in row["keywords"].split(","):
                        tmp_key.append(x.upper())
                                   
                    for i in fieldnames_publication :    

                        publi[i] = [row[i]]
                        publi["nbr_authors"]=str(nb_aut)
                        
                    ls_publication.append([publi])
                    publi = {}

                    for i in fieldnames_year :    
                        if row["year"] not in tmp_year:
                            if row[i] is not None :
                                year["year"] = [row[i]]
                                year["id_year"] = ["id_" + row[i]]
                                tmp_year.append(row["year"])
                                ls_year.append([year])
                                year = {}    
                            
                    for i in fieldnames_venue :    
                        if row["venue"] not in tmp_venue:
                            if row[i] is not None :
                                venue["venue"] = [row[i]]
                                venue["id_venue"] = ["id_" + row[i]]
                                tmp_venue.append(row["venue"])
                                ls_venue.append([venue])
                                venue = {}    

                    for i in fieldnames_publi_author :                        
                        publi_author[i] = [row[i]]
                        
                    ls_publi_author.append([publi_author])
                    publi_author = {}
                    
                    x = row["keywords"].split(",")
                    
                   
                    tmp = [row["id_publication"]]
                        
                    for j in x:
                        y = x.count(j)
                        tmp_nb_key[j] = y 

                    for i in tmp_nb_key.keys():    

                        publi_keywords["id_publication"] = tmp
                        publi_keywords["keywords"] = [i]
                        publi_keywords["nbr_use_keyword"] = [str(tmp_nb_key[i])]    
            
                        ls_publi_keywords.append([publi_keywords])
                        publi_keywords = {} 
                        
                    for i in fieldnames_publi_venue :                        
                        publi_venue[i] = [row[i]]
                        
                    ls_publi_venue.append([publi_venue])
                    
                    publi_venue = {}
                
                    for i in fieldnames_publi_year :                        
                        publi_year[i] = [row[i]]
                        
                    ls_publi_year.append([publi_year])
                    publi_year = {}

                    for i in fieldnames_venue :    
                        if row["year"] not in tmp_year:
                            if row[i] is not None :
                                year["year"] = [row[i]]
                                year["id_year"] = ["id_" + row[i]]
                                tmp_year.append(row["year"])
                                ls_year.append([year])
                                year = {} 
# =============================================================================
#                     exporter(mdict, col_imp, filename2)
# =============================================================================
                         
        elif critere1 == "publ":
            for row in reader:
                if critere2 in row["title"]:
                    for i in col_imp:
                        mdict[i] = row[i]
                    
                    with open('export2.csv', 'a', newline ='', encoding="utf-16") as csvfile:
                         writer = csv.DictWriter(csvfile, ["author","title","mdate","type","venue","id_publication", "year"], delimiter = ";")
                         writer.writerow(mdict)
  
                    for i in fieldnames_aut:   
                        nb_aut = len(row[i].split(","))
                        
                    for j in row[i].split(","):
                        tmp_aut.append(j)

                    for x in row["keywords"].split(","):
                        tmp_key.append(x.upper())
                                   
                    for i in fieldnames_publication :    

                        publi[i] = [row[i]]
                        publi["nbr_authors"]=str(nb_aut)
                        
                    ls_publication.append([publi])
                    publi = {}

                    for i in fieldnames_year :    
                        if row["year"] not in tmp_year:
                            if row[i] is not None :
                                year["year"] = [row[i]]
                                year["id_year"] = ["id_" + row[i]]
                                tmp_year.append(row["year"])
                                ls_year.append([year])
                                year = {}    
                            
                    for i in fieldnames_venue :    
                        if row["venue"] not in tmp_venue:
                            if row[i] is not None :
                                venue["venue"] = [row[i]]
                                venue["id_venue"] = ["id_" + row[i]]
                                tmp_venue.append(row["venue"])
                                ls_venue.append([venue])
                                venue = {}    

                    for i in fieldnames_publi_author :                        
                        publi_author[i] = [row[i]]
                        
                    ls_publi_author.append([publi_author])
                    publi_author = {}
                    
                    x = row["keywords"].split(",")
                    
                   
                    tmp = [row["id_publication"]]
                        
                    for j in x:
                        y = x.count(j)
                        tmp_nb_key[j] = y 

                    for i in tmp_nb_key.keys():    

                        publi_keywords["id_publication"] = tmp
                        publi_keywords["keywords"] = [i]
                        publi_keywords["nbr_use_keyword"] = [str(tmp_nb_key[i])]    
            
                        ls_publi_keywords.append([publi_keywords])
                        publi_keywords = {} 
                        
                    for i in fieldnames_publi_venue :                        
                        publi_venue[i] = [row[i]]
                        
                    ls_publi_venue.append([publi_venue])
                    
                    publi_venue = {}
                
                    for i in fieldnames_publi_year :                        
                        publi_year[i] = [row[i]]
                        
                    ls_publi_year.append([publi_year])
                    publi_year = {}

                    for i in fieldnames_venue :    
                        if row["year"] not in tmp_year:
                            if row[i] is not None :
                                year["year"] = [row[i]]
                                year["id_year"] = ["id_" + row[i]]
                                tmp_year.append(row["year"])
                                ls_year.append([year])
                                year = {} 
                                
        elif critere1 == "venue":
            for row in reader:
                if critere2 in row["venue"]:
                    for i in col_imp:
                        mdict[i] = row[i]
                    
                    with open('export2.csv', 'a', newline ='', encoding="utf-16") as csvfile:
                         writer = csv.DictWriter(csvfile, ["author","title","mdate","type","venue","id_publication", "year"], delimiter = ";")
                         writer.writerow(mdict)
  
                    for i in fieldnames_aut:   
                        nb_aut = len(row[i].split(","))
                        
                    for j in row[i].split(","):
                        tmp_aut.append(j)

                    for x in row["keywords"].split(","):
                        tmp_key.append(x.upper())
                                   
                    for i in fieldnames_publication :    

                        publi[i] = [row[i]]
                        publi["nbr_authors"]=str(nb_aut)
                        
                    ls_publication.append([publi])
                    publi = {}

                    for i in fieldnames_year :    
                        if row["year"] not in tmp_year:
                            if row[i] is not None :
                                year["year"] = [row[i]]
                                year["id_year"] = ["id_" + row[i]]
                                tmp_year.append(row["year"])
                                ls_year.append([year])
                                year = {}    
                            
                    for i in fieldnames_venue :    
                        if row["venue"] not in tmp_venue:
                            if row[i] is not None :
                                venue["venue"] = [row[i]]
                                venue["id_venue"] = ["id_" + row[i]]
                                tmp_venue.append(row["venue"])
                                ls_venue.append([venue])
                                venue = {}    

                    for i in fieldnames_publi_author :                        
                        publi_author[i] = [row[i]]
                        
                    ls_publi_author.append([publi_author])
                    publi_author = {}
                    
                    x = row["keywords"].split(",")
                    
                   
                    tmp = [row["id_publication"]]
                        
                    for j in x:
                        y = x.count(j)
                        tmp_nb_key[j] = y 

                    for i in tmp_nb_key.keys():    

                        publi_keywords["id_publication"] = tmp
                        publi_keywords["keywords"] = [i]
                        publi_keywords["nbr_use_keyword"] = [str(tmp_nb_key[i])]    
            
                        ls_publi_keywords.append([publi_keywords])
                        publi_keywords = {} 
                        
                    for i in fieldnames_publi_venue :                        
                        publi_venue[i] = [row[i]]
                        
                    ls_publi_venue.append([publi_venue])
                    
                    publi_venue = {}
                
                    for i in fieldnames_publi_year :                        
                        publi_year[i] = [row[i]]
                        
                    ls_publi_year.append([publi_year])
                    publi_year = {}

                    for i in fieldnames_venue :    
                        if row["year"] not in tmp_year:
                            if row[i] is not None :
                                year["year"] = [row[i]]
                                year["id_year"] = ["id_" + row[i]]
                                tmp_year.append(row["year"])
                                ls_year.append([year])
                                year = {} 
                        
    for i in tmp_key :
        tmp_dict_keywords[i] = tmp_key.count(i)
    
    for i in tmp_aut :
        tmp_dict_aut[i] = tmp_aut.count(i) 
    
    with open('keyword.csv', 'w', newline = "", encoding="utf-16") as csv_file:  
        writer = csv.writer(csv_file, delimiter = ";")
        writer.writerow(["keyword", "nbr_utilisation"])
        for key, value in tmp_dict_keywords.items():
           writer.writerow([key, value])

    with open('author.csv', 'w', newline = "", encoding="utf-16") as csv_file:  
        writer = csv.writer(csv_file, delimiter = ";")
        writer.writerow(["author", "nbr_publication"])
        for key, value in tmp_dict_aut.items():
           writer.writerow([key, value])

    fieldnames_publication = ["id_publication", "mdate", "title", "type", "nbr_authors"]
    exporter(ls_publication, fieldnames_publication, "publication.csv")
    exporter(ls_year, fieldnames_year, "year.csv")
    exporter(ls_publi_author, fieldnames_publi_author, "publication_author.csv")
    exporter(ls_publi_keywords, fieldnames_publi_keywords, "publication_keywords.csv")
    exporter(ls_publi_venue, fieldnames_publi_venue, "publication_venue.csv")
    exporter(ls_publi_year, fieldnames_publi_year, "publication_year.csv")
    exporter(ls_publi_venue, fieldnames_publi_venue, "venue.csv")


def exporter(doc, tags, filename):
    import csv

    with open(filename, 'w', newline ='', encoding="utf-16") as csvfile:
        writer = csv.DictWriter(csvfile, tags, delimiter = ";")
        if os.stat(filename).st_size == 0 :
           writer.writeheader()
        for i in doc:
            for row in i :
                val = dict((k, ",".join(map(str,v))) for k, v in row.items())
                writer.writerow(val)

#test("publ","machine learning")