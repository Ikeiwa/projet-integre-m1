from datetime import datetime
import os

filename = "export.csv"

try:
    os.remove(filename)
except OSError:
    pass

try:
    os.remove("auteur.csv")
except OSError:
    pass
try:
    os.remove("author2.csv")
except OSError:
    pass

startTime = datetime.now()
    
def importer(nomfichier):
    from lxml import etree as ET
    import nltk
    from nltk.corpus import stopwords
    from nltk.tokenize import RegexpTokenizer

    stop_words = stopwords.words('english')
    stop_words_fr = stopwords.words('french')
    nltk.download("stopwords")
    tokenizer = RegexpTokenizer(r'\w+')
    
    tree = ET.iterparse(nomfichier, events = ('end',)
                        , tag = ('article', 'book', 'incollection', 'inproceedings'
                                 , 'proceedings', 'phdthesis', 'masterthesis', 'www'
                                 ,'person', 'data')
                        , dtd_validation = True, recover = True)
    
    tags = ["author","title","journal", "key", "mdate", "type", "keywords"
            , "id_journal","id_author", "year", "id_year"]
    header = ["author","title","venue", "key", "mdate", "type", "keywords"
            , "id_venue","id_author", "year", "id_year"]
    
    ls = []
    ls2 = []
    mdict = {}

    #boucle qui va parcourir chaque balise du fichier xml
    for event, elem in tree:
        
        mdict["type"] = [elem.tag]
        #La fonction items permet de récupérer les informations écrites à l'intérieur des balises
        #Exemple: <article key ="bla"> elle récupère key : bla dans un dictionnaire
        for i in range (0,len(elem.items())):
            
            #On stocke cette information dans une liste                
            if elem.items()[i][0] in tags :

                if elem.items()[i][0] in mdict: 
                    
                    mdict[elem.items()[i][0]].append(elem.items()[i][1])

                else:

                    mdict[elem.items()[i][0]] = [elem.items()[i][1]]

        for i in elem :
            
            ls.append([i.tag,i.text])

        for i in ls:
            
            # si l'element de ls est un auteur on rajoute le nom de l'auteur dans un dictionnaire
            # qui sera une ligne du fichier auteur csv. On incremente le nombre d'auteur que l'on ecrira aussi
            # dans ce fichier csv. On sauvegarde toutes ces informations dans une liste ce qui nous permet
            # de reduire grandement le temps de calcul            
            if i[0] in tags :
                
                if i[0] in mdict: 
                    mdict[i[0]].append(i[1])
                else:
                    mdict[i[0]] = [i[1]]
                    
        mdict["keywords"] = []
        if "title" in mdict.keys() :
            for i in mdict["title"]:
                if i is not None : 
                    i = tokenizer.tokenize(i)
                    for j in i:
                        if j.lower() not in stop_words and j.lower() not in stop_words_fr:
                            mdict["keywords"].append(j)
            
        mdict["id_journal"] = []
        if "journal" in mdict.keys() :
            for i in mdict["journal"] : 
                if i is not None :
                    mdict["id_journal"].append("id_" + i)
        
        mdict["id_author"] = []
        if "author" in mdict.keys() :
            for i in mdict["author"]:
                if i is not None:
                    mdict["id_author"].append("id_" + i)
                    
        mdict["id_year"] = []
        if "year" in mdict.keys() :
            for i in mdict["year"]:
                if i is not None:
                    mdict["id_year"].append("id_" + i)

        ls = []
        ls2.append([mdict])

        mdict = {}

        elem.clear()
        
        if len(ls2) > 1000 :
            exporter(ls2, tags, "export.csv", header)
            ls2 = []         
            
    return (ls)


def exporter(doc, tags, filename, header):
    import csv

    with open(filename, 'a', newline ='', encoding="utf-16") as csvfile:
        writer = csv.DictWriter(csvfile, tags, delimiter = ";")
        if os.stat(filename).st_size == 0 :
                wtr = csv.writer(csvfile, delimiter =";")    
                wtr.writerow(header)
        for i in doc:
            for row in i :
                val = dict((k, ",".join(map(str,v))) for k, v in row.items())
                writer.writerow(val)



print("Temps d'éxecution : ", datetime.now() - startTime)   


            
